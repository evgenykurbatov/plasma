# -*- coding: utf-8 -*-

import numpy as np
from numpy import pi, sqrt, exp, sin, cos, tan, log, log10

import plasma
from plasma.const import *



p = plasma.Plasma()

print("kappa_es   = %.2e (cm^2 g^(-1))" % (p.kappa_es))
print("kappa_ff_P = %.2e N/T^3.5 (cm^2 g^(-1))" % (6.90e-24/m_p))
print("kappa_ff_R = %.2e N/T^3.5 (cm^2 g^(-1))" % (6.58e22*m_p))

rho = 1.0  ## g cm^(-3)
T = 1e7    ## K
H = 1e6    ## cm
N_i = rho/p.m_irel
sigma_R = p.kappa_es * rho
alpha_R = p.kappa_ff_R(N_i, T) * rho
tau_es_R = sigma_R * 2*H
tau_ff_R = alpha_R * 2*H
print()
print("rho = %.2e (g cm^(-3))" % rho)
print("H = %.2e (cm)" % H)
print("T = %.2e (K)" % T)
print("N_i = %.2e (cm^(-3))" % N_i)
print("sigma_R = %.2e (cm^(-1))" % sigma_R)
print("alpha_R = %.2e (cm^(-1))" % alpha_R)
print("tau_es_R = %.2e" % tau_es_R)
print("tau_ff_R = %.2e" % tau_ff_R)
print("tau_R = %.2e" % (tau_es_R + tau_ff_R))

rho *= 10**(-33.0/20)
T *= 10**(-9.0/10)
H *= 10
N_i = rho/p.m_irel
sigma_R = p.kappa_es * rho
alpha_R = p.kappa_ff_R(N_i, T) * rho
tau_es_R   = sigma_R * 2*H
tau_ff_R   = alpha_R * 2*H
print()
print("rho = %.2e (g cm^(-3))" % rho)
print("H = %.2e (cm)" % H)
print("T = %.2e (K)" % T)
print("N_i = %.2e (cm^(-3))" % N_i)
print("sigma_R = %.2e (cm^(-1))" % sigma_R)
print("alpha_R = %.2e (cm^(-1))" % alpha_R)
print("tau_es_R = %.2e" % tau_es_R)
print("tau_ff_R = %.2e" % tau_ff_R)
print("tau_R = %.2e" % (tau_es_R + tau_ff_R))
