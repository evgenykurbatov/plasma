======
Plasma
======

:Author:
   Evgeny P. Kurbatov
:Contact:
   evgeny.p.kurbatov at gmail dot com


Git repository: https://bitbucket.org/evgenykurbatov/plasma



Intro
=====


In this package various coefficients from plasma physics are given: characteristic scales, times, cross-sections, opacities etc.
