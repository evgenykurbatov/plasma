# -*- coding: utf-8 -*-


def atos(a, limit=None):
    fmt = lambda x : (" %.2e" % x) if abs(log10(x)) >= 2 else (" %.3g" % x)
    s = "["
    if len(a) > 0:
        s += fmt(a[0])
    if limit and 2*limit > len(a):
        for x in a[1:limit]:
            s += "," + fmt(x)
        s += " ..."
        for x in a[-limit:]:
            s += "," + fmt(x)
    else:
        for x in a[1:]:
            s += "," + fmt(x)
    s += " ]"
    return s
