# -*- coding: utf-8 -*-

import numpy as np
from numpy import pi, sqrt, exp, sin, cos, tan, log, log10

from . const import *



class Kinetics(object):
    """
    Quasi-neutral single-temperature plasma kinetics.
    """


    def __init__(self):
        ## Gas constant
        ## [pressure] = [density] [gas constant] [temperature]
        self.gas_const = k_B/self.m_mean


    ##
    ## Thermodynamics
    ##

    def mass_density(self, N_i):
        """Mass density"""
        return self.m_irel * N_i

    def rho(self, N_i):
        """Mass density"""
        return self.mass_density(N_i)


    def gas_pressure(self, N_i, T):
        """Gas pressure"""
        return (self.Z+1)*N_i * k_B*T

    def P_gas(self, N_i, T):
        """Gas pressure"""
        return self.gas_pressure(N_i, T)


    ##
    ## Non-equillibrium processes
    ##

    def thermal_vel_e(self, T):
        """Electrons thermal velocity"""
        return sqrt(k_B*T/(3.0*m_e))

    def v_th_e(self, T):
        """Electrons thermal velocity"""
        return self.thermal_vel_e(T)


    def thermal_vel_i(self, T):
        """Ions thermal velocity"""
        return sqrt(k_B*T/(3.0*self.m_i))

    def v_th_i(self, T):
        """Ions thermal velocity"""
        return self.thermal_vel_i(T)


    def plasma_freq(self, N_i):
        """Plasma (or fundamental, or Langmuir) frequency"""
        return sqrt(self.Z*(self.Z+1) * 4*pi*e**2*N_i/m_e)

    def omega_L(self, N_i):
        """Plasma (or fundamental, or Langmuir) frequency"""
        return self.plasma_freq(N_i)


    def debye_length(self, N_i, T):
        """Debye length"""
        return sqrt(k_B*T/m_e) / self.plasma_freq(N_i)

    def a_D(self, N_i, T):
        """Debye length"""
        return self.debye_length(N_i, T)


    def scatter_ei(self, N_i, T):
        """Electron-ion scatter cross-section"""
        return 4*pi * ( self.Z * e**2/(3*k_B*T))**2 \
            * log( self.debye_length(N_i, T) * 3*k_B*T/(self.Z * e**2) )


    def scatter_ii(self, N_i, T):
        """Ion-ion scatter cross-section"""
        return 4*pi * ( self.Z**2 * e**2/(3*k_B*T))**2 \
            * log( self.debye_length(N_i, T) * 3*k_B*T/(self.Z**2 * e**2) )


    def mean_free_path_ei(self, N_i, T):
        """Free path length in electron-ion collisions"""
        return 1.0 / (self.scatter_ei(N_i, T) * N_i)


    def mean_free_path_ii(self, N_i, T):
        """Free path length in ion-ion collisions"""
        return 1.0 / (self.scatter_ii(N_i, T) * N_i)


    def mean_free_time_ei(self, N_i, T):
        """Free path length in electron-ion collisions"""
        return self.mean_free_path_ei(N_i, T) / self.thermal_vel_e(T)


    def mean_free_time_ii(self, N_i, T):
        """Free path length in electron-ion collisions"""
        return self.mean_free_path_ii(N_i, T) / self.thermal_vel_i(T)


    def ohm_conduct(self, N_i, T):
        """Ohm conductivity"""
        return e**2/m_e * self.Z*N_i * self.mean_free_time(N_i, T)


    ##
    ## Magnetic processes
    ##

    def magnetic_pressure(self, B):
        """Magnetic pressure"""
        return B**2/(8*pi)

    def P_mag(self, B):
        return self.magnetic_pressure(B)


    def cyclotron_freq_e(self, B):
        """Cyclotronic frequency for electrons"""
        return e*B/(m_e*c)

    def omega_B_e(self, B):
        """Cyclotronic frequency for electrons"""
        return cyclotron_freq_e(B)


    def cyclotron_freq_i(self, B):
        """Cyclotronic frequency for ions"""
        return e*B/(self.m_i*c)

    def omega_B_i(self, B):
        """Cyclotronic frequency for ions"""
        return cyclotron_freq_i(B)


    def cyclotron_radius_e(self, T, B):
        """Cyclotronic radius for electrons"""
        return self.thermal_vel_e(T) * 2*pi/self.cyclotron_freq_e(B)

    def r_B_e(self, T, B):
        """Cyclotronic radius for electrons"""
        return cyclotron_radius_e(T, B)


    def cyclotron_radius_i(self, T, B):
        """Cyclotronic radius for ions"""
        return self.thermal_vel_i(T) * 2*pi/self.cyclotron_freq_i(B)

    def r_B_i(self, T, B):
        """Cyclotronic radius for ions"""
        return cyclotron_radius_i(T, B)


    def razin_freq(self, N_i, B):
        """Razin frequency"""
        return 2*pi * 20.0*self.Z*N_i/B


    def alfven_vel(self, N_i, B):
        """Alvfen velocity"""
        return B / sqrt(4*pi*self.mass_density(N_i))

    def v_alf(self, N_i, B):
        """Alvfen velocity"""
        return alfven_vel(N_i, B)


    def ohm_diffusion(self, N_i, T):
        """Ohm diffusion coefficient"""
        return c**2 / (4*pi*self.ohm_conduct(N_i, T))


    def bohm_diffusion(self, T, B):
        """Bohm diffusion coefficient"""
        return (1/16) * c*k_B*T/(e*B)
