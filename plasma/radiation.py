# -*- coding: utf-8 -*-

import numpy as np
from numpy import pi, sqrt, exp, sin, cos, tan, log, log10
import scipy.special

from . const import *



class Radiation(object):
    """
    Radiation transfer in quasi-neutral single-temperature plasma.
    """


    def __init__(self):
        ## Electron-electron cross section [cm^2]
        self.sigma_es = 8*pi/3 * (e**2/(m_e*c**2))**2
        ## Thomson opacity [cm^2/g]
        self.kappa_es = self.sigma_es/self.m_irel

        ## Free-free cross-section factor
        self.kappa_ff_const = 4*pi*e**6/(3*m_e**2*c*h) * sqrt(2*m_e/(3*pi*k_B))


    def planck(self, T, nu):
        """Planck function"""
        return 2*h/c**2 * nu**3 / scipy.special.expm1(h*nu/(k_B*T))


    def gaunt_ff(self, T, nu):
        """Gaunt factor for free-free transitions"""
        return sqrt(3.0)/pi * scipy.special.k0e(0.5 * h*nu/(k_B*T))


    def kappa_ff(self, N_i, T, nu):
        """Free-free opacity [cm^2/g]"""
        return self.kappa_ff_const/self.m_irel * self.Z*N_i/sqrt(T) \
            * self.gaunt_ff(T, nu)/nu**3 * (-scipy.special.expm1(-h*nu/(k_B*T)))


    def kappa_ff_P(self, N_i, T):
        """Planck-averaged free-free opacity [cm^2/g]"""
        return 6.90e-24/self.m_irel * self.Z*N_i/T**3.5


    def kappa_ff_R(self, N_i, T):
        """Rayleigh-averaged free-free opacity [cm^2/g]"""
        return 6.58e22*self.m_irel * self.Z*N_i/T**3.5
