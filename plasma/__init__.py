# -*- coding: utf-8 -*-

from . aux import *
from . const import *
from . kinetics import *
from . radiation import *


__version__ = "0.1"



class Plasma(Kinetics, Radiation):
    """
    Quasi-neutral plasma, N_e = Z*N_i.

    Parameters
    ----------
    Z : integer
        Atomic number. Default is 1.
    A : integer
        Mass number. Default is 1.

    Properties
    ----------
    m_i : float
        Ion mass.
    m_irel : float
        Mass of a particle related to ion number density, i.e. rho/N_i
    m_mean : float
        Mean particle mass
    """

    def __init__(self, Z=1, A=1):
        assert Z <= A, "Atomic number (Z) must be lesser or equal than the mass number (A)!"
        self.Z = float(Z)
        self.A = float(A)

        ## Ion mass
        self.m_i = self.Z*m_p + (self.A - self.Z)*m_n

        ## Mass of a particle related to ion number density, i.e. rho/N_i
        self.m_irel = self.Z*m_e + self.m_i

        ## Mean particle mass
        self.m_mean = self.m_irel / (self.Z+1)


        ##
        ## Initialize physical models
        ##

        Kinetics.__init__(self)
        Radiation.__init__(self)
